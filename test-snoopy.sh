#!/bin/bash

module purge
module load PrgEnv-gnu/7.1

set -ex

gcc -Wall -m64 -fPIC -shared -o snoopy.so snoopy.c -ldl -lm
gcc test-snoopy.c -o test-snoopy

LD_PRELOAD=./snoopy.so ./test-snoopy
echo "Exit status $?"
